import React, {Component} from 'react';
import {View, Text} from 'react-native';
import PropTypes from 'prop-types';
import {StyleSheet, Text, View} from 'react-native';

export default class Header extends Component {
    static propTypes = {
        title: PropTypes.string,
    };

    render() {
        const {title} = this.props;
        return (
            <View>
                <Text style={styles.header}>{title}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 10,
        textAlign: 'center',
    },
});
