import {StyleSheet} from 'react-native';

const PARALLAX_HEADER_HEIGHT = 150;
const STICKY_HEADER_HEIGHT = 40;

export default StyleSheet.create({
    flag: {
        borderWidth: 1,
        borderColor: '#c7c6c6',
        flex: 1,
        height: 22,
        width: 32,
    },
    stickySection: {
        height: STICKY_HEADER_HEIGHT,
    },
    stickySectionText: {
        backgroundColor: '#008558',
        color: 'white',
        fontSize: 20,
        height: STICKY_HEADER_HEIGHT,
        paddingLeft: 10,
        paddingTop: 8,
    },
    badgeContainer: {
        flex: 1,
    },
    badge: {
        alignSelf: 'center',
        height: 150,
        justifyContent: 'center',
        marginTop: 20,
        width: 150,
    },
    statsWrapper: {
        flex: 1,
        flexDirection: 'row',
    },
    statsContainer: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'column',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
    },
    statDescription: {
        color: 'gray',
        fontSize: 12,
    },
    statValue: {
        color: '#D64545',
        fontSize: 30,
        fontWeight: 'bold',
    },
    playersWrapper: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 20,
        marginTop: 20,
    },
    playerContainer: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'column',
        marginTop: 20,
    },
    playerName: {
        color: '#D64545',
        fontSize: 18,
        fontWeight: 'bold',
    },
    playerPosition: {
        color: '#D64545',
        fontSize: 14,
    },
    playerAge: {
        color: '#D64545',
        fontSize: 12,
    },
    playerImage: {
        flex: 1,
        height: 150,
        width: 150,
    },
});
