import React, {PureComponent} from 'react';
import {StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import {withNavigation} from 'react-navigation';

class TeamItem extends PureComponent {
    static propTypes = {
        item: PropTypes.object,
        navigation: PropTypes.shape({
            navigate: PropTypes.func,
        }),
    };

    static defaultProps = {
        item: {},
        navigation: undefined,
    };

    goToDetails = teamCode => {
        const {navigation} = this.props;
        navigation.navigate('TeamDetails', {teamCode});
    };

    render() {
        const {item} = this.props;
        return (
            <TouchableOpacity
                onPress={() => this.goToDetails(item.code)}
                style={styles.itemContainer}>
                <Image style={styles.flag} source={{uri: item.flag}} />
                <Text style={styles.item}>{item.name}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    flag: {
        borderWidth: 1,
        borderColor: '#c7c6c6',
        flex: 1,
        height: 22,
        width: 32,
    },
    item: {
        flex: 3,
        paddingLeft: 10,
    },
    itemContainer: {
        alignItems: 'center',
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'row',
        marginBottom: 10,
        marginRight: 10,
        paddingLeft: 10,
        paddingRight: 10,
        paddingVertical: 10,
    },
});

export default withNavigation(TeamItem);
