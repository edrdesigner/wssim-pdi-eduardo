import React, {PureComponent} from 'react';
import {StyleSheet, View, FlatList, ActivityIndicator} from 'react-native';
import TeamItem from './TeamItem';
import teamsApi from '../api/TeamsApi';

export default class TeamList extends PureComponent {
    state = {
        countries: [],
        loading: true,
    };

    async componentDidMount() {
        const countries = await teamsApi.getAll();
        this.setState({countries, loading: false});
    }

    renderItem = item => {
        return <TeamItem item={item.item} />;
    };

    render() {
        const {loading, countries} = this.state;

        if (loading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            );
        } else {
            return (
                <FlatList
                    data={countries}
                    numColumns={2}
                    keyExtractor={item => item.code}
                    renderItem={this.renderItem}
                />
            );
        }
    }
}

const styles = StyleSheet.create({
    loading: {
        flex: 1,
        padding: 20,
    },
});
