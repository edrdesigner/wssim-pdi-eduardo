import React, {PureComponent} from 'react';
import {Image, View, Text, FlatList, Dimensions} from 'react-native';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import PropTypes from 'prop-types';
import styles from './TeamDetails.style';

const PARALLAX_HEADER_HEIGHT = 150;
const STICKY_HEADER_HEIGHT = 40;
const window = Dimensions.get('window');

export default class TeamDetails extends PureComponent {
    static propTypes = {
        team: PropTypes.shape({
            badge: PropTypes.string,
            code: PropTypes.string,
            flag: PropTypes.string,
            name: PropTypes.string,
            profileImage: PropTypes.string,
            stats: PropTypes.shape({
                appearances: PropTypes.number,
                final: PropTypes.number,
                rank: PropTypes.number,
                semifinals: PropTypes.number,
                titles: PropTypes.number,
            }),
            players: PropTypes.arrayOf(
                PropTypes.shape({
                    name: PropTypes.string,
                    position: PropTypes.string,
                    age: PropTypes.number,
                    img: PropTypes.string,
                })
            ),
        }).isRequired,
    };

    static defaultProps = {
        team: {
            stats: {},
            players: [],
        },
    };

    render() {
        const {team} = this.props;

        const background = () => (
            <View key="background">
                <Image
                    source={{
                        uri: team.profile_image,
                        width: window.width,
                        height: PARALLAX_HEADER_HEIGHT,
                    }}
                />
                <View
                    style={{
                        position: 'absolute',
                        top: 0,
                        width: window.width,
                        height: PARALLAX_HEADER_HEIGHT,
                    }}
                />
            </View>
        );

        const sticky = () => (
            <View key="sticky-header" style={styles.stickySection}>
                <Text style={styles.stickySectionText}>{team.name}</Text>
            </View>
        );

        const renderPlayer = player => (
            <View style={styles.playerContainer}>
                <Image style={styles.playerImage} source={{uri: player.img}} />
                <Text style={styles.playerName}>{player.name}</Text>
                <Text style={styles.playerPosition}>{player.position}</Text>
                <Text style={styles.playerAge}>{player.age}</Text>
            </View>
        );

        return (
            <View style={{flex: 1, backgroundColor: 'white'}}>
                <ParallaxScrollView
                    backgroundColor="black"
                    parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT}
                    stickyHeaderHeight={STICKY_HEADER_HEIGHT}
                    renderBackground={background}
                    renderStickyHeader={sticky}>
                    <View style={styles.badgeContainer}>
                        <Image
                            style={styles.badge}
                            source={{uri: team.badge}}
                            resizeMode="contain"
                        />
                    </View>
                    <View style={styles.statsWrapper}>
                        <View style={styles.statsContainer}>
                            <Text style={styles.statValue}>
                                {team.stats.rank}
                            </Text>
                            <Text style={styles.statDescription}>
                                Ranking na FIFA
                            </Text>
                        </View>
                        <View style={styles.statsContainer}>
                            <Text style={styles.statValue}>
                                {team.stats.appearances}
                            </Text>
                            <Text style={styles.statDescription}>
                                Presenças na Copa
                            </Text>
                        </View>
                    </View>
                    <View style={styles.statsWrapper}>
                        <View style={styles.statsContainer}>
                            <Text style={styles.statValue}>
                                {team.stats.titles}
                            </Text>
                            <Text style={styles.statDescription}>Títulos</Text>
                        </View>
                        <View style={styles.statsContainer}>
                            <Text style={styles.statValue}>
                                {team.stats.semifinals}
                            </Text>
                            <Text style={styles.statDescription}>
                                Semifinais
                            </Text>
                        </View>
                        <View style={styles.statsContainer}>
                            <Text style={styles.statValue}>
                                {team.stats.final}
                            </Text>
                            <Text style={styles.statDescription}>Finais</Text>
                        </View>
                    </View>
                    <View style={styles.playersWrapper}>
                        <FlatList
                            data={team.players}
                            numColumns={2}
                            keyExtractor={item => item.name}
                            renderItem={({item}) => renderPlayer(item)}
                        />
                    </View>
                </ParallaxScrollView>
            </View>
        );
    }
}
