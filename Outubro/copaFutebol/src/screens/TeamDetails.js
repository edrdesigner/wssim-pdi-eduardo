import React, {Component} from 'react';
import {View, ActivityIndicator} from 'react-native';
import teamsApi from '../api/TeamsApi';
import TeamDetails from './../components/TeamDetails';
import PropTypes from 'prop-types';

export default class TeamDetailsScreen extends Component {
    static propTypes = {
        navigation: PropTypes.object,
    };

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            team: {},
        };
        this.routeParams = props.navigation.state.params;
    }

    async componentWillMount() {
        const team = await teamsApi.get(this.routeParams.teamCode);
        this.setState({team, loading: false});
    }

    render() {
        const {loading, team} = this.state;
        if (loading) {
            return (
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            );
        } else {
            return <TeamDetails team={this.state.team} />;
        }
    }
}
