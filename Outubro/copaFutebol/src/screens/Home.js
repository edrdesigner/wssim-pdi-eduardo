import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import TeamList from '../components/TeamList';

export default class Home extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TeamList />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#f4f5f9',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        paddingBottom: 25,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 25,
    },
});
