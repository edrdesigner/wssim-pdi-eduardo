import React, {Component} from 'react';
import {View, Image} from 'react-native';

export default class Foo extends Component {

    render() {
        return (
            <View>
                <Image source={{uri:'https://raw.githubusercontent.com/adam-p/markdown-here/master/src/common/images/icon48.png'}} />
            </View>
        );
  }
}
