import 'react-native';
import Foo from '../Foo';
import React from 'react';
import renderer from 'react-test-renderer';

describe('Foo component', () => {
    it ('shoud have same snapshot', () => {
        const foo = renderer.create(<Foo />).toJSON();
        expect(foo).toMatchSnapshot();
    });
});