
const assert = require('assert');
const counter = require('../src/counter');

describe('Testando contador', () => {
    beforeEach(() => {
        counter.count = 0;
    });

    it('contador deve iniciar com valor zero', () => {
        const counterResult = counter.count;
        assert.equal(counterResult, 0);
    });

    it('teste de incremento de 1', () => {
        counter.increment();
        const counterResult = counter.count;
        assert.equal(counterResult, 1);
    });

    it('teste de decremento de 1', () => {
        counter.decrement();
        const counterResult = counter.count;
        assert.equal(counterResult, -1);
    });

    it('teste de incremento de 10', () => {
        counter.increment(20);
        const counterResult = counter.count;
        assert.equal(counterResult, 20);
    });
});