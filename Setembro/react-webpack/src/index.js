import React from "react";
import ReactDOM from "react-dom";
// we now have some nice styles
import "index.scss";

let HelloWorld = () => {
    return (<h1>Hello there world!! </h1>);
}

ReactDOM.render(
    <HelloWorld />,
    document.getElementById("root")
)