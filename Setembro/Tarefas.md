
# PDI Setembro

## Criar um aplicativo React Native com aplicação de testes unitários
 - Comecarei este mês

## REACT NATIVE
Estou estudando a documentação e vendo o que posso fazer com este framework

## Testes unitários
 - Exemplo feito

## TEDS ASSISTIDOS

### Por que devemos buscar a perfeição e parar de ter medo do fracasso
Este TED fala da questão de evitar falhas a todo custo e ser perfeito em tudo que se faz, pois exemplo na medicina um erro ou erro aceitável pode custar uma vida.

Devemos não ser bons suficientes 99% devemos ser 100% perfeitos e para isto devemos treinar e praticar a falha para que não falhemos e se falharmos, devemos aprender com este erro para não repeti-lo jamais.

E e nas pequenas coisas que devemos ser pefeitos pois se algo falha nas pequenas coisas em um processo o todo pode ser falho. Devemos procurar a perfeição, e não nos contentarmos com o 'bom o suficiente'.

Podemos falhar mas não devemos nunca desistir.

### Os 7 Sinais De Perfeccionismo Exagerado
Este artigo trás o lado negativo de ser um perfeccionista exagerado e como evita-los.

### Overcoming your Insecurity (Superenado sua inseguranças)
Neste talk Jung Lee da Coréia do Sul fala sobre a insegurança das pessoas
principalmente nas escolas, onde as crianças se sentem inseguras consigo mesmas, seja pelo tipo que se vestem, pelo seu tipo físico etc, e como podemos
superar estas inseguranças.

Bom apartir do momento que você se sente bem consigo mesmo, quando você no seu interior se sente bem e sabe disso, mesmo que o mundo exterior não reflita isto você estará bem.

Um dos motivos que iniciei a meditação, foi a questão de me auto conhecer, saber o que esta acontecendo la dentro da caixa que me faz pensar nas ações que todo diáriamente.

### Insecurities: What Makes or Breaks Us
Caleb neste TED conta um pouco sobre sua história de insegurança que deu motivação para se tornar melhor.
Ele era sedentário e acomodado com sua vida e como ela estava indo.
Então em janeiro de 2014 ele resolveu mudar de vida. Se esforçando e se motivando para driblar suas inseguranças.

Acredito que podemos nos tornar pessoas melhores 'configurando nosso mindset'
para tal, se motivando todos os dias e buscar ser uma pessoa melhor, conhecendo suas fraquesas e lutando para vence-las.

## Tirar 15 minutos toda manha para organizar suas tarefas no dia, pessoas com uma organização maior do seu tempo conseguem aproveitá-lo melhor, o que reduz muitos fatores causadores de ansiedade.
Comecei a ter 15 minutos de planejamento todas as manhãs para melhorar meu dia e produtividade
foi muito bom saber o que tinha que fazer deixando tudo bem organizado.

## Comecar a praticar algum exercicio regularmente, onde voce possa se desligar completamente assim reduzindo seu estresse diário.
Após leitura do livro 'O Milagre da Manhã' comecei a acordar todos os dias, exceto finais de semana
as 05:15 da manhã neste período tinha meu tempo para:
- Tomar aguá com limão 500ml
- Meditar
- Comer algo simples como uma bolacha com doce de amendoin ou uma fruta
- Ir para a acâdemia das 06:00 às 07:00h após banho agua normal para animar

Resultados: Estou melhorando meu condicionamento físico, estou indo dormir mais cedo e não ficar mais vendo
vídeos até tarde da noite, para acordar cedo você deve dormir cedo e consegue.
Meu dia começa com a missão de me tornar uma pessoa melhor em suas primeiras horas assim, chego no trabalho
empolgado sabendo que já fiz várias coisas antes de começar a programar.

